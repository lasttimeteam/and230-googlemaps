﻿using System.Threading.Tasks;
using Android.App;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using DroidMapping;

namespace Maps
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : Activity, IOnMapReadyCallback
    {
        static readonly LatLng Location_Xamarin = new LatLng(37.80, -122.40);
        static readonly LatLng Location_NewYork = new LatLng(40.77, -73.98);
        static readonly LatLng Location_Chicago = new LatLng(41.88, -87.63);
        static readonly LatLng Location_Dallas = new LatLng(32.90, -97.03);

        MappingPermissionsHelper permissionHelper;
        private Task getLocationPermissionsAsync;
        private MapFragment mapFragment;
        private GoogleMap map;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            permissionHelper = new MappingPermissionsHelper(this);
            getLocationPermissionsAsync = permissionHelper.CheckAndRequestPermissions();

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            mapFragment = FragmentManager.FindFragmentById(Resource.Id.map) as MapFragment;

            mapFragment.GetMapAsync(this);
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
           base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
           permissionHelper.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public async void OnMapReady(GoogleMap googleMap)
        {
            map = googleMap;
            map.AddMarker(new MarkerOptions().SetPosition(Location_NewYork));

            map.AddMarker(new MarkerOptions()
                .SetPosition(Location_Xamarin)
                .SetTitle("Xamarin HQ")
                .SetSnippet("Where the magic happens")
                .SetIcon(BitmapDescriptorFactory
                    .FromResource(Resource.Drawable.xamarin_icon)));

          var chicagoMarker = map.AddMarker(new MarkerOptions()
                .SetPosition(Location_Chicago)
                .SetTitle("Chicago")
                .Draggable(true)
                .SetIcon(BitmapDescriptorFactory
                    .DefaultMarker(BitmapDescriptorFactory.HueYellow)));

           var dallasMarker = map.AddMarker(new MarkerOptions()
                .SetPosition(Location_Dallas)
                .SetTitle("Dallas")
                .SetSnippet("Go Cowboys!")
                .InfoWindowAnchor(1, 0)
                .SetIcon(BitmapDescriptorFactory
                    .DefaultMarker(BitmapDescriptorFactory.HueBlue)));

           map.MarkerClick += (o, e) =>
           {
               if (e.Marker.Equals(dallasMarker))
               {
                   dallasMarker.Flat = !dallasMarker.Flat;
                   dallasMarker.ShowInfoWindow();
               }
               else
               {
                   e.Handled = false;
               }
           };

           map.InfoWindowClick += (o, e) =>
           {
               if (e.Marker.Id == chicagoMarker.Id)
               {
                   e.Marker.SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueRose));
               }
           };

           map.MapClick += (o, e) =>
           {
               if (!chicagoMarker.IsInfoWindowShown)
               {
                   chicagoMarker.SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueYellow));
               }
           };

           map.MapLongClick += (o, e) =>
           {
               map.AnimateCamera(CameraUpdateFactory.ZoomOut(), 1000, null);
           };

           CameraUpdate update = CameraUpdateFactory.NewLatLngZoom(Location_Xamarin, map.MaxZoomLevel);

           map.AnimateCamera(update);

            map.MapType = GoogleMap.MapTypeNormal;

            var hasLocationPermission = await permissionHelper.CheckAndRequestPermissions();
            map.MyLocationEnabled = hasLocationPermission;

            map.UiSettings.CompassEnabled = true;
            map.UiSettings.MyLocationButtonEnabled = true;
            map.UiSettings.ZoomControlsEnabled = true;
            map.UiSettings.SetAllGesturesEnabled(true);

            await getLocationPermissionsAsync;

            LatLng coord = new LatLng(37.80, -122.40);
            CameraUpdate updateCam = CameraUpdateFactory.NewLatLngZoom(coord, 17);
            map.AnimateCamera(updateCam);

        }
    }
}